/**
 * @dataManagerGroup             PlayCricket
 * @dataManagerGridFields        id,label,type,datecreated,datemodified
 * @datamanagerAllowedOperations read,viewversions
 */

component {
	property name="id"       type="string" dbtype="varchar" required=true maxlength=15 generator="none";
	property name="type"     type="string" dbtype="varchar" maxlength=50;
	property name="ecb_site" relationship="many-to-one" relatedTo="ecb_site";
}