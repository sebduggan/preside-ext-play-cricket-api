/**
 * @nolabel   true
 * @versioned false
 */

component {
	property name="ecb_site"       relationship="many-to-one" relatedTo="ecb_site" uniqueIndexes="ecb_sync|1";
	property name="season"         type="numeric" dbtype="int"                     uniqueIndexes="ecb_sync|2";
	property name="endpoint"       type="string"  dbtype="varchar" maxlength=50    uniqueIndexes="ecb_sync|3";
	property name="last_sync_date" type="date"    dbtype="datetime";
}