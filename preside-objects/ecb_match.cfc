/**
 * @nolabel                      true
 * @dataManagerGroup             PlayCricket
 * @dataManagerGridFields        id,match_date,match_time,team,opposition_club,opposition_team,last_updated
 * @datamanagerAllowedOperations read,viewversions
 */

component {
	property name="id"                  type="string"  dbtype="varchar" required=true maxlength=15 generator="none";
	property name="ecb_site"            relationship="many-to-one" relatedTo="ecb_site";

	property name="status"              type="string"  dbtype="varchar" maxlength=20 indexes="status";
	property name="published"           type="boolean" dbtype="boolean";
	property name="last_updated"        type="date"    dbtype="datetime";
	property name="team"                relationship="many-to-one" relatedTo="ecb_team";
	property name="opposition_club"     type="string"  dbtype="varchar" maxlength=100;
	property name="opposition_team"     type="string"  dbtype="varchar" maxlength=100;
	property name="venue"               type="string"  dbtype="char"    maxlength=1;
	property name="league"              relationship="many-to-one" relatedTo="ecb_league";
	property name="league_name"         adminRenderer="none" type="string"  dbtype="varchar" maxlength=100;
	property name="competition"         relationship="many-to-one" relatedTo="ecb_competition";
	property name="competition_name"    adminRenderer="none" type="string"  dbtype="varchar" maxlength=100;
	property name="competition_type"    type="string"  dbtype="varchar" maxlength=50;
	property name="match_type"          type="string"  dbtype="varchar" maxlength=50;
	property name="game_type"           type="string"  dbtype="varchar" maxlength=50;
	property name="season"              type="numeric" dbtype="int" indexes="season";
	property name="match_date"          type="date"    dbtype="date";
	property name="match_time"          type="time"    dbtype="time";
	property name="ground_id"           type="string"  dbtype="varchar" maxlength=15;
	property name="ground_name"         type="string"  dbtype="varchar" maxlength=100;
	property name="ground_latitude"     type="numeric" dbtype="double";
	property name="ground_longitude"    type="numeric" dbtype="double";
	property name="home_club_name"      type="string"  dbtype="varchar" maxlength=100;
	property name="home_team_name"      type="string"  dbtype="varchar" maxlength=100;
	property name="home_team_id"        type="string"  dbtype="varchar" maxlength=15;
	property name="home_club_id"        type="string"  dbtype="varchar" maxlength=15;
	property name="away_club_name"      type="string"  dbtype="varchar" maxlength=100;
	property name="away_team_name"      type="string"  dbtype="varchar" maxlength=100;
	property name="away_team_id"        type="string"  dbtype="varchar" maxlength=15;
	property name="away_club_id"        type="string"  dbtype="varchar" maxlength=15;

	property name="umpire_1_name"       adminViewGroup="officials" type="string"  dbtype="varchar" maxlength=100;
	property name="umpire_1_id"         adminViewGroup="officials" type="string"  dbtype="varchar" maxlength=15;
	property name="umpire_2_name"       adminViewGroup="officials" type="string"  dbtype="varchar" maxlength=100;
	property name="umpire_2_id"         adminViewGroup="officials" type="string"  dbtype="varchar" maxlength=15;
	property name="umpire_3_name"       adminViewGroup="officials" type="string"  dbtype="varchar" maxlength=100;
	property name="umpire_3_id"         adminViewGroup="officials" type="string"  dbtype="varchar" maxlength=15;
	property name="referee_name"        adminViewGroup="officials" type="string"  dbtype="varchar" maxlength=100;
	property name="referee_id"          adminViewGroup="officials" type="string"  dbtype="varchar" maxlength=15;
	property name="scorer_1_name"       adminViewGroup="officials" type="string"  dbtype="varchar" maxlength=100;
	property name="scorer_1_id"         adminViewGroup="officials" type="string"  dbtype="varchar" maxlength=15;
	property name="scorer_2_name"       adminViewGroup="officials" type="string"  dbtype="varchar" maxlength=100;
	property name="scorer_2_id"         adminViewGroup="officials" type="string"  dbtype="varchar" maxlength=15;

	property name="toss_won_by_team_id" adminViewGroup="result" type="string"  dbtype="varchar" maxlength=15;
	property name="toss"                adminViewGroup="result" type="string"  dbtype="varchar" maxlength=250;
	property name="batted_first"        adminViewGroup="result" type="string"  dbtype="varchar" maxlength=15;
	property name="no_of_overs"         adminViewGroup="result" type="numeric" dbtype="int";
	property name="no_of_innings"       adminViewGroup="result" type="numeric" dbtype="int";
	property name="result"              adminViewGroup="result" type="string"  dbtype="varchar" maxlength=5;
	property name="result_description"  adminViewGroup="result" type="string"  dbtype="varchar" maxlength=250;
	property name="result_applied_to"   adminViewGroup="result" type="string"  dbtype="varchar" maxlength=15;
	property name="match_notes"         adminViewGroup="result" type="string"  dbtype="text";

	property name="result_short"        adminViewGroup="result" type="string"  dbtype="varchar" maxlength=20;
}