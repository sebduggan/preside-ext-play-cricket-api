/**
 * @dataManagerGroup                PlayCricket
 * @dataManagerGridFields           id,label,first_season,api_token
 * @datamanagerDisallowedOperations delete,clone
 */

component {
	property name="id"           type="string"  dbtype="varchar" required=true maxlength=15 generator="none";
	property name="slug"         type="string"  dbtype="varchar" required=true maxlength=50;
	property name="api_token"    type="string"  dbtype="varchar" required=true maxlength=50;
	property name="first_season" type="numeric" dbtype="int"     required=true;
}