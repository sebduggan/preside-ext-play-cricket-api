<cf_presideparam name="args.match_date"      />
<cf_presideparam name="args.match_time"      />
<cf_presideparam name="args.opposition_club" />
<cf_presideparam name="args.opposition_team" />
<cf_presideparam name="args.ground_name"     />
<cf_presideparam name="args.venue"           />

<cfscript>
	oppositionName = args.opposition_club;
	if ( ( args.opposition ?: "" ) == "clubteam" ) {
		oppositionName &= " " & args.opposition_team;
	}
</cfscript>
<cfoutput>

	<tr>
		<td>#dateformat( args.match_date, "ddd d mmm yyyy" )#</td>
		<td>#timeformat( args.match_time, "h:mm" )#</td>
		<td>#args.venue#</td>
		<td>#oppositionName#</td>
		<td>#args.ground_name#</td>
	</tr>

</cfoutput>