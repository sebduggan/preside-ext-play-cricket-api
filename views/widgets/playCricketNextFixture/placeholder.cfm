<cfparam name="args.title" default="" />
<cfparam name="args.teams" default="" />
<cfparam name="args.season" default="" />

<cfscript>
	title = len( trim( args.title ) ) ? args.title : "Next fixture";
</cfscript>

<cfoutput>
	<strong>#title#</strong>
	<cfloop list="#args.teams#" index="team">
		<br>
		#renderLabel( objectName="ecb_team", recordId=team, labelRenderer="playCricketTeamWithSite" )#
	</cfloop>
</cfoutput>