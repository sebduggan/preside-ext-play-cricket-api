<cfparam name="args.title" default="" />

<cfscript>
	title           = len( trim( args.title ) ) ? args.title : "Next fixture";
	renderedFixture = args.renderedFixture ?: "";
</cfscript>

<cfoutput>

	<h2>#title#</h2>

	<cfif len( trim( renderedFixture ) )>
		<table>
			<thead>
				<tr>
					<th>Date</th>
					<th>Time</th>
					<th>Venue</th>
					<th>Opposition</th>
					<th>Ground</th>
				</tr>
			</thead>
			<tbody>
				#renderedFixture#
			</tbody>
		</table>
	<cfelse>
		<p>No upcoming fixtures found.</p>
	</cfif>

</cfoutput>