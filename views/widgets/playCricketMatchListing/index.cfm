<cfparam name="args.title" default="" />

<cfscript>
	instanceId       = createUUID();
	previousSeasons  = isTrue( args.prev_seasons ?: "" );
	availableSeasons = args.availableSeasons ?: [];
	renderedFixtures = args.renderedFixtures ?: "";
	displaySeason    = args.season           ?: "";
</cfscript>

<cfoutput>

	<h2>#args.title#</h2>

	<cfif previousSeasons>
		<form
			class="fixture-table-season-select"
			action="#event.getCurrentUrl()#"
			method="get"
			data-fixture-table-id="#instanceId#"
			data-teams="#args.teams#"
			data-ajax-url="#event.buildLink( linkTo="widgets.playCricketMatchListing.ajaxRequest" )#">
			#renderFormControl(
				  name         = "season"
				, type         = "select"
				, context      = "website"
				, defaultValue = displaySeason
				, values       = availableSeasons.reverse()
				, layout       = "formcontrols.layouts.field.website"
				, label        = "Show fixtures for season:"
			)#
			<button class="btn-small">Apply</button>
		</form>
	</cfif>

	<table class="table-ruled" id="fixture-table-#instanceId#">
		<thead>
			<tr>
				<th>Date</th>
				<th>Time</th>
				<th>Venue</th>
				<th>Opposition</th>
				<th>Ground</th>
				<th>Result</th>
			</tr>
		</thead>
		<tbody>
			<cfif len( trim( renderedFixtures ) )>
				#renderedFixtures#
			<cfelse>
				#renderView( "/widgets/playCricketMatchListing/_noFixtures" )#
			</cfif>
		</tbody>
	</table>

</cfoutput>