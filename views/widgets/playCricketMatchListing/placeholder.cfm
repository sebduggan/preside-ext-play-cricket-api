<cfparam name="args.title"        default="" />
<cfparam name="args.teams"        default="" />
<cfparam name="args.season"       default="" />
<cfparam name="args.prev_seasons" default="" />

<cfoutput>
	<strong>#args.title#</strong><br>
	<cfloop list="#args.teams#" index="team">
		#renderLabel( objectName="ecb_team", recordId=team, labelRenderer="playCricketTeamWithSite" )#<br>
	</cfloop>
	<strong>Season:</strong> <cfif isNumeric( args.season )>#args.season#<cfelse>current</cfif>
	<cfif isTrue( args.prev_seasons )>
		<br>
		<em class="grey">Show previous seasons</em>
	</cfif>
</cfoutput>