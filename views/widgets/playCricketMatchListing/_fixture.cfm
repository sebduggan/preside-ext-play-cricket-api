<cf_presideparam name="args.id"              />
<cf_presideparam name="args.match_date"      />
<cf_presideparam name="args.match_time"      />
<cf_presideparam name="args.opposition_club" />
<cf_presideparam name="args.opposition_team" />
<cf_presideparam name="args.ground_name"     />
<cf_presideparam name="args.venue"           />
<cf_presideparam name="args.result_short"    />
<cf_presideparam name="args.site_slug" field="ecb_site.slug" />

<cfscript>
	resultUrl      = "https://#args.site_slug#.play-cricket.com/website/results/#args.id#";
	oppositionName = args.opposition_club;
	if ( ( args.opposition ?: "" ) == "clubteam" ) {
		oppositionName &= " " & args.opposition_team;
	}
</cfscript>
<cfoutput>

	<tr>
		<td>#dateformat( args.match_date, "ddd d mmm yyyy" )#</td>
		<td>#timeformat( args.match_time, "h:mm" )#</td>
		<td>#args.venue#</td>
		<td>#oppositionName#</td>
		<td>#args.ground_name#</td>
		<td><cfif len( args.result_short )><a href="#resultUrl#" target="_blank">#args.result_short#</a></cfif></td>
	</tr>

</cfoutput>