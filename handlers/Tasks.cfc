/**
 * EMS specific tasks for the task manager
 *
 */
component {
	property name="playCricketSyncService" inject="playCricketSyncService";

	/**
	 * Sync latest data from PlayCricket
	 *
	 * @displayName  Sync latest data from PlayCricket
	 * @displayGroup Play Cricket
	 * @schedule     0 05 *\/2 * * *
	 * @priority     10
	 * @timeout      1200
	 */
	public boolean function syncPlayCricketLatest( any logger ) {
		return playCricketSyncService.doSync(
			  logger  = arguments.logger ?: nullValue()
			, syncAll = false
		);
	}

	/**
	 * Sync all data from PlayCricket
	 *
	 * @displayName  Sync all data from PlayCricket
	 * @displayGroup Play Cricket
	 * @schedule     disabled
	 * @priority     10
	 * @timeout      1200
	 */
	public boolean function syncPlayCricketAll( any logger ) {
		return playCricketSyncService.doSync(
			  logger  = arguments.logger ?: nullValue()
			, syncAll = true
		);
	}

}