component {

	private array function _selectFields( event, rc, prc ) {
		return [
			  "ecb_team.label as teamName"
			, "ecb_site.label as siteName"
		];
	}

	private string function _orderBy( event, rc, prc ) {
		return "ecb_site.label, ecb_team.label";
	}

	private string function _renderLabel( event, rc, prc ) {
		var siteName = arguments.siteName ?: "";
		var teamName = arguments.teamName ?: "";

		return htmlEditFormat( siteName ) & " - " & htmlEditFormat( teamName );
	}

}