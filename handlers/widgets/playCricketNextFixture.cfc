component {

	property name="presideobjectservice" inject="presideobjectservice";

	private function index( event, rc, prc, args={} ) {
		var filter       = "team in ( :team ) and match_date >= curdate()";
		var filterParams = { team=listToArray( args.teams ) };

		args.renderedFixture = renderView(
			  view          = "/widgets/playCricketNextFixture/_fixture"
			, presideObject = "ecb_match"
			, filter        = filter
			, filterParams  = filterParams
			, orderBy       = "match_date, match_time, team.label"
			, maxRows       = 1
			, args          = args
		);

		return renderView( view="widgets/playCricketNextFixture/index", args=args );
	}

	private function placeholder( event, rc, prc, args={} ) {
		return renderView( view="widgets/playCricketNextFixture/placeholder", args=args );
	}
}
