component {

	property name="playCricketListingService" inject="PlayCricketListingService";

	private function index( event, rc, prc, args={} ) {
		args.season = isNumeric( rc.season ?: "" ) ? rc.season : args.season;
		if ( !isNumeric( args.season ) ) {
			args.season = getSystemSetting(
				  category = "playcricket"
				, setting  = "ecb_current_season"
				, default  = year( now() )
			);
		}

		var filter  = {
			  season = args.season
			, team   = listToArray( args.teams )
		};
		args.renderedFixtures = renderView(
			  view          = "/widgets/playCricketMatchListing/_fixture"
			, presideObject = "ecb_match"
			, filter        = filter
			, orderBy       = "match_date, team.label"
			, args          = args
		);

		if ( isTrue( args.prev_seasons ?: "" ) ) {
			args.availableSeasons = playCricketListingService.getAvailableSeasons( args.teams, args.season );
		}

		return renderView( view="widgets/playCricketMatchListing/index", args=args );
	}

	private function placeholder( event, rc, prc, args={} ) {
		return renderView( view="widgets/playCricketMatchListing/placeholder", args=args );
	}

	public string function ajaxRequest( event, rc, prc ) {
		var season = rc.season ?: "";
		var teams  = listToArray( rc.teams  ?: "" );

		if ( !len( season ) || !teams.len() ) {
			return renderView( "/widgets/playCricketMatchListing/_noFixtures" );
		}

		var renderedFixtures = renderView(
			  view          = "/widgets/playCricketMatchListing/_fixture"
			, presideObject = "ecb_match"
			, filter        = {
				  season = season
				, team   = teams
			  }
			, orderBy       = "match_date, team.label"
		);

		if ( len( trim( renderedFixtures ) ) ) {
			return renderedFixtures;
		}
		return renderView( "/widgets/playCricketMatchListing/_noFixtures" );

	}
}
