/**
 * @presideService
 * @singleton
 */
component {
// CONSTRUCTOR
	/**
	 *
	 */
	public any function init() {
		return this;
	}

	public array function getMatches(
		  required numeric siteId
		, required numeric season
		,          date    updatedFrom
		,          date    updatedTo
	) {
		var params = _baseApiParams( arguments.siteId );

		params.season = arguments.season;

		if ( isDate( arguments.updatedFrom ?: "" ) ) {
			params.from_entry_date = dateFormat( arguments.updatedFrom, "dd/mm/yyyy" );
		}
		if ( isDate( arguments.updatedTo ?: "" ) ) {
			params.to_entry_date = dateFormat( arguments.updatedTo, "dd/mm/yyyy" );
		}

		var response = _apiCall( endpoint="matches", params=params );

		return response.matches ?: [];
	}

	public array function getResults(
		  required numeric siteId
		, required numeric season
		,          date    updatedFrom
		,          date    updatedTo
	) {
		var params = _baseApiParams( arguments.siteId );

		params.season = arguments.season;

		if ( isDate( arguments.updatedFrom ?: "" ) ) {
			params.from_entry_date = dateFormat( arguments.updatedFrom, "dd/mm/yyyy" );
		}
		if ( isDate( arguments.updatedTo ?: "" ) ) {
			params.to_entry_date = dateFormat( arguments.updatedTo, "dd/mm/yyyy" );
		}

		var response = _apiCall( endpoint="result_summary", params=params );

		return response.result_summary ?: [];
	}


	private struct function _apiCall( required string endpoint, required struct params ) {
		var apiUrl      = "https://play-cricket.com/api/v2/#arguments.endpoint#.json";
		var httpRequest = new HTTP();

		httpRequest.setUrl( apiUrl );
		httpRequest.setMethod( "GET" );
		httpRequest.setCharset( "utf-8" );
		httpRequest.setTimeout( 30 );

		for( var paramName in arguments.params ) {
			httpRequest.addParam( type="url", name=paramName, value=arguments.params[ paramName ] );
		}

		var httpResponse   = httpRequest.send().getPrefix();
		var statusCode     = httpResponse.status_code ?: "";
		var responseBody   = httpResponse.fileContent ?: "";
		var responseObject = {};

		try {
			responseObject = DeSerializeJson( responseBody );
		} catch ( any e ) {
			throw(
				  type         = "PlayCricketApi.invalid_response"
				, message      = "An unexpected and invalid response was received from the PlayCricket API request. See error detail and extended info for full response details."
				, detail       = responseBody
				, extendedInfo = SerializeJson( arguments.httpResponse )
				, errorCode    = statusCode
			);
		}

		return responseObject;
	}


	private struct function _baseApiParams( required numeric siteId ) {
		var site = $getPresideObject( "ecb_site" ).selectData( id=arguments.siteId );

		return { site_id=site.id, api_token=site.api_token };
	}
}