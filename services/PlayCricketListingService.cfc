/**
 * @presideService
 * @singleton
 */
component {
// CONSTRUCTOR
	/**
	 *
	 */
	public any function init() {
		return this;
	}

	public array function getAvailableSeasons( required string teams, numeric season ) {
		var seasons          = $getPresideObject( "ecb_match" ).selectData(
			  filter       = { team=listToArray( arguments.teams ) }
			, distinct     = true
			, orderBy      = "season"
			, selectFields = [ "season" ]
		);
		var availableSeasons = seasons.valueArray( "season" );

		if ( isNumeric( arguments.season ?: "" ) && !availableSeasons.find( arguments.season ) ) {
			availableSeasons = availableSeasons.append( arguments.season ).sort( "numeric" );
		}

		return availableSeasons;
	}

}