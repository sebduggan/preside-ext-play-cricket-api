/**
 * @presideService
 * @singleton
 */
component {
// CONSTRUCTOR
	/**
	 * @apiWrapper.inject  PlayCricketApiWrapper
	 */
	public any function init( required any apiWrapper ) {
		_setApiWrapper( arguments.apiWrapper );
		return this;
	}


	public boolean function doSync( boolean syncAll=false, any logger ) {
		var hasLogger = StructKeyExists( arguments, "logger" );
		var canInfo   = hasLogger && arguments.logger.canInfo();

		var sites = $getPresideObject( "ecb_site" ).selectData();
		if ( canInfo ) { arguments.logger.info( "#sites.recordcount# site(s) found..." ); }

		for( var site in sites ) {
			if ( canInfo ) { arguments.logger.info( "Start syncing [#site.label#] - site id [#site.id#]" ); }
			for( var season=site.first_season; season <= year( now() ); season++ ) {
				_syncMatches(
					  siteId = site.id
					, season = season
					, syncAll = arguments.syncAll
					, logger  = arguments.logger ?: nullValue()
				);
				_syncResults(
					  siteId = site.id
					, season = season
					, syncAll = arguments.syncAll
					, logger  = arguments.logger ?: nullValue()
				);
			}
			if ( canInfo ) { arguments.logger.info( "Finish syncing [#site.label#]" ); }
		}
		if ( canInfo ) { arguments.logger.info( "All finished." ); }

		return true;
	}


	private void function _syncMatches(
		  required numeric siteId
		, required numeric season
		,          boolean syncAll=false
		,          any     logger
	) {
		var hasLogger = StructKeyExists( arguments, "logger" );
		var canInfo   = hasLogger && arguments.logger.canInfo();
		var dao       = $getPresideObject( "ecb_match" );
		var endpoint  = "matches";
		var args      = duplicate( arguments );
		var deleted   = [];

		if ( !arguments.syncAll ) {
			args.updatedFrom = _getLastSync( siteId=arguments.siteId, season=arguments.season, endpoint=endpoint );
		}

		var matches = _getApiWrapper().getMatches( argumentCollection=args );
		if ( canInfo ) { arguments.logger.info( "#matches.len()# matches found for #arguments.season# season" ); }

		for( var match in matches ) {
			if ( match.status == "Deleted" ) {
				deleted.append( match.id );
				continue;
			}

			_decorateMatch( siteId, match );
			match.last_sync_match = now();

			if ( dao.dataExists( id=match.id ) ) {
				dao.updateData( id=match.id, data=match );
			} else {
				dao.insertData( data=match );
			}
		}

		_deleteMatches( matchIds=deleted, logger=arguments.logger ?: nullValue() );

		if ( canInfo ) { arguments.logger.info( "Finish syncing matches for #arguments.season# season" ); }

		_setLastSync( siteId=arguments.siteId, season=arguments.season, endpoint=endpoint )
	}

	private void function _syncResults(
		  required numeric siteId
		, required numeric season
		,          boolean syncAll=false
		,          any     logger
	) {
		var hasLogger = StructKeyExists( arguments, "logger" );
		var canInfo   = hasLogger && arguments.logger.canInfo();
		var dao       = $getPresideObject( "ecb_match" );
		var endpoint  = "result_summary";
		var args      = duplicate( arguments );
		var deleted   = [];

		if ( !arguments.syncAll ) {
			args.updatedFrom = _getLastSync( siteId=arguments.siteId, season=arguments.season, endpoint=endpoint );
		}

		var matches = _getApiWrapper().getResults( argumentCollection=args );
		if ( canInfo ) { arguments.logger.info( "#matches.len()# results found for #arguments.season# season" ); }

		for( var match in matches ) {
			if ( match.status == "Deleted" ) {
				deleted.append( match.id );
				continue;
			}

			_decorateMatch( siteId, match );
			match.last_sync_result = now();

			if ( dao.dataExists( id=match.id ) ) {
				dao.updateData( id=match.id, data=match );
			} else {
				dao.insertData( data=match );
			}
		}

		_deleteMatches( matchIds=deleted, logger=arguments.logger ?: nullValue() );

		if ( canInfo ) { arguments.logger.info( "Finish syncing results for #arguments.season# season" ); }

		_setLastSync( siteId=arguments.siteId, season=arguments.season, endpoint=endpoint )
	}



	private any function _getLastSync(
		  required numeric siteId
		, required numeric season
		, required string  endpoint
	) {
		var sync = $getPresideObject( "ecb_sync" ).selectData(
			filter = {
				  ecb_site = arguments.siteId
				, season   = arguments.season
				, endpoint = arguments.endpoint
			}
		);
		if ( sync.recordCount ) {
			return sync.last_sync_date;
		}

		return;
	}

	private void function _setLastSync(
		  required numeric siteId
		, required numeric season
		, required string  endpoint
	) {
		var dao     = $getPresideObject( "ecb_sync" );
		var updated = dao.updateData(
			  filter = {
				  ecb_site = arguments.siteId
				, season   = arguments.season
				, endpoint = arguments.endpoint
			  }
			, data   = { last_sync_date=now() }
		);
		if ( !updated ) {
			dao.insertData(
				data = {
					  ecb_site       = arguments.siteId
					, season         = arguments.season
					, endpoint       = arguments.endpoint
					, last_sync_date = now()
				}
			);
		}
	}

	private void function _deleteMatches( required array matchIds, any logger ) {
		var hasLogger = StructKeyExists( arguments, "logger" );
		var canInfo   = hasLogger && arguments.logger.canInfo();
		var deleted   = $getPresideObject( "ecb_match" ).deleteData( filter={ id=arguments.matchIds } );

		if ( canInfo && deleted ) { arguments.logger.info( "#deleted# records deleted" ); }
	}

	private numeric function _insertTeam( required numeric siteId, required struct match ) {
		var teamId   = "";
		var teamName = "";
		var dao      = $getPresideObject( "ecb_team" );

		if ( match.home_club_id == arguments.siteId ) {
			teamId   = match.home_team_id;
			teamName = match.home_team_name;
		} else if ( match.away_club_id == arguments.siteId ) {
			teamId   = match.away_team_id;
			teamName = match.away_team_name;
		} else {
			return;
		}

		var team = dao.selectData( id=teamId );
		if ( !team.recordCount ) {
			dao.insertData( data={ id=teamId, label=teamName, ecb_site=arguments.siteId } );
		} else if ( team.label != teamName ) {
			dao.updateData( id=teamId, data={ label=teamName } );
		}

		return teamId;
	}

	private numeric function _insertCompetition( required numeric siteId, required struct match ) {
		var dao = $getPresideObject( "ecb_competition" );

		if ( !isNumeric( match.competition_id ?: "" ) ) {
			return;
		}

		var competition = dao.selectData( id=match.competition_id );
		if ( !competition.recordCount ) {
			dao.insertData( data={ id=match.competition_id, label=match.competition_name, type=match.competition_type, ecb_site=arguments.siteId } );
		} else if ( competition.label != match.competition_name || competition.type != match.competition_type ) {
			dao.updateData( id=match.competition_id, data={ label=match.competition_name, type=match.competition_type } );
		}

		return match.competition_id;
	}

	private numeric function _insertLeague( required numeric siteId, required struct match ) {
		var dao = $getPresideObject( "ecb_league" );

		if ( !isNumeric( match.league_id ?: "" ) ) {
			return;
		}

		var league = dao.selectData( id=match.league_id );
		if ( !league.recordCount ) {
			dao.insertData( data={ id=match.league_id, label=match.league_name, ecb_site=arguments.siteId } );
		} else if ( league.label != match.league_name ) {
			dao.updateData( id=match.league_id, data={ label=match.league_name } );
		}

		return match.league_id;
	}

	private void function _decorateMatch( required numeric siteId, required struct match ) {
		var teamId = "";

		match.ecb_site     = arguments.siteId;
		match.team         = _insertTeam( arguments.siteId, match );
		match.competition  = _insertCompetition( arguments.siteId, match );
		match.league       = _insertLeague( arguments.siteId, match );
		match.last_updated = _parseDate( match.last_updated );
		match.match_date   = _parseDate( match.match_date );

		if ( match.ground_name == "Add New Ground" ) {
			match.ground_name = "";
		}

		if ( match.home_club_id == arguments.siteId ) {
			teamId                = match.home_team_id;
			match.venue           = "H";
			match.opposition_club = match.away_club_name;
			match.opposition_team = match.away_team_name;
		} else if ( match.away_club_id == arguments.siteId ) {
			teamId                = match.away_team_id;
			match.venue           = "A";
			match.opposition_club = match.home_club_name;
			match.opposition_team = match.home_team_name;
		}

		if ( len( match.result ?: "" ) ) {
			switch( match.result ) {
				case "W":
					match.result_short = ( match.result_applied_to == teamId ) ? "Won" : "Lost";
					break;
				case "CON":
					match.result_short = ( match.result_applied_to == teamId ) ? "Conceded (won)" : "Conceded (lost)";
					break;
				case "C":
					match.result_short = "Cancelled";
					break;
				case "A":
					match.result_short = "Abandoned";
					break;
				case "D":
					match.result_short = "Drawn";
					break;
				case "T":
					match.result_short = "Tied";
					break;
				case "M":
					match.result_short = "In progress...";
					break;
				default:
					match.result_short = "";
			}

		}
	}

	private any function _parseDate( required string dateString ) {
		var date = listToArray( arguments.dateString, "/" );

		if ( date.len() != 3 ) {
			return;
		}

		return createDate( date[3], date[2], date[1] );
	}


 // GETTERS AND SETTERS
	private any function _getApiWrapper() {
		return _apiWrapper;
	}
	private void function _setApiWrapper( required any apiWrapper ) {
		_apiWrapper = arguments.apiWrapper;
	}

}